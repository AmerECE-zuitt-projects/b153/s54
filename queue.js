let collection = [];
let head = 0;
let rear = 0;

// Write the queue functions below.

const print = () => {
  return collection;
};

const enqueue = (element) => {
  collection[rear] = element;
  rear++;
  return collection;
};

const dequeue = () => {
  let items = collection[head];
  delete collection[head];
  head++;
  return items;
};

const front = () => {
  return collection[head];
};

const size = () => {
  return collection.length();
};

const isEmpty = () => {
  if (collection.length() === 0) {
    return true;
  } else {
    return false;
  }
};

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
